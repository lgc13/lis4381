LIS 4381 - Mobile Web Application Development

##### Assignments:

1)[A1 README.md](a1/README.md)

  * Install AMPPS
  * Install JDK
  * Install Android Studio and create My First App
  * Provide screenshots of installations
  * Create a Bitbucket tutorials
  * Provide git command description

2)[A2 README.md](a2/README.md)

  * Create Healthy Recipes Android App
  * Use native programming to create the two-page App
  * Use radio buttons, images, and text
  * Provide screenshots of completed app

3)[A3 README.md](a3/README.md)

  * Create ERD based upon business rules
  * Provide screenshot of completed ERD
  * Provide DB resource links

4)[A4 README.md](a4/README.md)

  * Create index page, with carousel
  * Create working links for other folders
  * Create P1 index page
  * Validate user input to each specific type
  * Give error messages when a user inputs wrong values
  * Show user when input has been properly inputed

5)[A5 README.md](a5/README.md)

  * Use validation to fully submit user input
  * Create fully functional delete and edit buttons for the database
  * Give error message upon wrong server side validation

6)[Project 1: README.md](project1/README.md)

  * Create a personal business card App
  * Use native programming for Android for the two-page App
  * Add background color for the App title, and main two pages
  * Change avatar picture
  * Use radio buttons, images and text

7)[Project 2: README.md](project2/README.md)

  * Create a server side validation for 'petstore' database
  * Upload database with
