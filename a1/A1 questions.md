Part 3
Answer the following questions (Chs. 1, 2):
1. You create line comments in PHP code by adding to a line you want to use as a
comment. (Choose all that apply.)
a. ||
b. **
        c . #
        d. //
2. Block comments begin with /* and end with _____.
a. /*
          b. */
c. //
d. **
3. PHP variables begin with _____________________.
 a. &
 b. !
          c. $
 d. #
4. A variable name in PHP can only contain letters, numbers, and
_______________________.
                    a. underscores
  b. percent signs
  c. pound symbols
  d. caret symbols
5. A PHP variable name
                                  a. is case-sensitive
b. can contain special characters
c. can start with a letter, a digit, or two underscores
d. can be a PHP reserved word
6. A web application is a type of
                          a. client/server application
b. database application
c. client-side application
d. server-side application
7. After the if statement that follows is executed, what will the value of $discountAmount
be?
$discount_amount;
$order_total = 200;
if ($order_total > 200) {
 $discount_amount = $order_total * .3;
} else if ($order_total > 100) {
 $discount_amount = $order_total * .2;
} else {
 $discount_amount = $order_total * .1;
}
 a. 0 b. 20 c. 40
                                d. 60
8. How many times will the while loop that follows be executed?
$months = 5;
$i = 1;
while ($i > $months) {
 $futureValue = $futureValue * (1 + $monthlyInterestRate);
 $i = $i+1;
}
a. 0
b. 1
                    c. 4
d. 5

9. If a URL specifies a directory that doesn’t contain a default page, Apache displays
                                                  a. an error message
b. a list of the PHP applications in that directory
c. a list of all of the web applications in that directory
d. a list of all of the directories in that directory
10. In the code that follows, if $error_message isn’t empty
 if ($error_message != '') {
 include('index.php');
 exit();
 }
                                  a. control is passed to a page named index.php that’s in the current directory
b. control is passed to a page named index.php that’s in the parent directory
c. control is passed to a page named index.php that’s in the child directory
d. nothing happens because the condition is false
11. The HTTP response for a dynamic web page is passed
a. from PHP to the browser
                          b. from the web server to the browser
c. from the database server to the web server
d. from the web server to PHP
12. The order of precedence for arithmetic expressions causes
a. addition to be done before subtraction
b. multiplication to be done before division
                  c. increment operations to be done first
d. modulus operations to be done last
13. To round and format the value of a variable named $number to 3 decimal places and
store it in a variable named $number_formatted, you code
a. $number_formatted = number_format($number, 3, round);
              b. $number_formatted = number_format($number, 3);
c. $number_formatted = format_number($number, 3, round);
d. $number_formatted = format_number($number, 3);
14. To run a PHP application that has been deployed on your own computer, you can enter a
URL in the address bar of your browser that
a. specifies just the application root directory
                  b. uses localhost as the domain name
c. specifies just the application root directory and the starting filename
d. uses apache as the domain name
15. To view the source code for a web page in the Firefox or IE browser, you can select the
appropriate command from the
 a. File menu
                b. Source menu
c. View menu
d. Page menu
16. When the web server recieves an HTTP request for a PHP page, the web server calls the
                            a. database server
 b. static web page
 c. web browser
 d. PHP interpreter
17. Which of the following is NOT part of an HTTP URL:
 a. protocol
 b. path
            c. node
  d. filename
8
18. What will the variable $name contain after the code that follows is executed?
$first_name = 'Bob';
$last_name = 'Roberts';
$name = "Name: $first_name";
a. Bob
b. $first_name
                      c. Name: Bob
d. Name: $first_name
19. In PHP, the concatenation operator is
a. +
b. &
                  c. .
d. !
20. In a conditional expression that includes logical operators, the AND operator has a lower
order of precedence than the ______________ operator.
          a. NOT
b. OR
c. ||
d. &&
