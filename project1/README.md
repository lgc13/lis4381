> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381 - Mobile Web Application Development

## Lucas Costa

### Project 1 Requirements:

1. Course title, your name, assignment requirements, as per A1;
2. Screenshot of running application’s first user interface;
3. Screenshot of running application’s second user interface;


#### README.md file should include the following items:

* Screenshot of running application’s first user interface;
* Screenshot of running application’s second user interface;

> This is a blockquote.
>
> This is the second paragraph in the blockquote.


##### Assignment Screenshots:

* Screenshot of ERD:

![ERD Screenshot](img/my_app.png)

![ERD Screenshot](img/my_app2.png)
